<div align="center"> 

# Microcontroller interfaced with display devices

</div>
<br>

## Aim
Interfacing of 8051 Microcontroller with various display devices.

### **About experiment –**

- This experiment includes the interfacing of LEDs and Seven segment display with 8051 Microcontroller.

- After completion of this experiment, students will be able to

    1. Interface an 8051 microcontroller with a display device and can perform the desired task.

    1. Program a 8051 microcontroller using assembly language.

<div align="center"> 

# Microcontroller interfaced with display devices

</div>
<br>

## Theory

### **Task 1:**
Blinking LED using 8051 Microcontroller.

### **Theory –**
A light-emitting diode (LED) is a semiconductor device or simply a PN junction diode that emits visible light when an electric current passes through it in the forward direction. The amount of light output is directly proportional to the forward current till a certain value of current.
![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/symbol%20and%20connection.jpg "Virtual Labs")



### **How Light Emitting Diode (LED) works?**
Light Emitting Diode (LED) works only in forward bias condition. When Light Emitting Diode (LED) is forward biased, the free electrons from n-side and the holes from p-side are pushed towards the junction.
When free electrons reach the junction or depletion region, some of the free electrons recombine with the holes in the positive ions. We know that positive ions have less number of electrons than protons. Therefore, they are ready to accept electrons. Thus, free electrons recombine with holes in the depletion region. In a similar way, holes from p-side recombine with electrons in the depletion region.

![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/photons_and_electrons.jpg "Virtual Labs")

                   Image source : www.physics-and-radio-electronics.com

Because of the recombination of free electrons and holes in the depletion region, the width of the depletion region decreases. As a result, more charge carriers will cross the p-n junction. Some of the charge carriers from p-side and n-side will cross the p-n junction before they recombine in the depletion region. For example, some free electrons from n-type semiconductor cross the p-n junction and recombine with holes in p-type semiconductor. In a similar way, holes from p-type semiconductor cross the p-n junction and recombine with free electrons in the n-type semiconductor. Thus, recombination takes place in the depletion region as well as in p-type and n-type semiconductor. The free electrons in the conduction band release energy in the form of light before they recombine with holes in the valence band.



### **How LED will be connected to 8051 practically?**
The below diagram shows the interfacing of a LED with a port pin of microcontroller Interfacing_of_led
interfacing_of_led
Image source : Designed on CADSOFT EAGLE tool
![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/interfacing_of_led.jpg "Virtual Labs")

### **Task 2:**
Display a digit on seven segment display using 8051 microcontrollers.
Theory –
The 7-segment display consists of seven LEDs arranged in a rectangular fashion. Each of the seven LEDs is called a segment because when illuminated the segment forms part of a numerical digit (both Decimal and Hex) to be displayed. An additional 8th LED is sometimes used within the same package which is the indication of a decimal point(DP), when two or more 7-segment displays are connected together numbers greater than ten can be displayed.
So by forward biasing the appropriate pins of the LED segments in a particular order, some segments will be glowing and others will remain as it Image source : aimagin.comis, allowing the desired character pattern of the number to be generated on the display. This then allows us to display each of the ten decimal digits 0 to 9 on the same 7-segment display.

Now accordingly terminals are taken common, so there are two types of display:
1. Common Cathode display
2. Common Anode display
   
### **1. The Common Cathode (CC) –**
In the common cathode display, all the cathode connections of the LED segments are joined together to logic “0” or ground. The individual segments are illuminated by application of a “HIGH”, or logic “1” signal via a current limiting resistor to forward bias the individual Anode terminals (a-g).
![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/common_cathode.jpg "Virtual Labs")
![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/common_cathode_table.jpg "Virtual Labs")
**common_cathode**
Image source : www.electronics-tutorials.ws

common_cathode_table

### **2. The Common Anode (CA) –**
In the common anode display, all the anode connections of the LED segments are joined together to logic “1”. The individual segments are illuminated by applying a ground, logic “0” or “LOW” signal via a current limiting resistor to the Cathode of the particular segment (a-g).
![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/common_anode.jpg "Virtual Labs")
Image source : www.electronics-tutorials.ws

![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/common_anode_table.jpg "Virtual Labs")
common_anode_table

In general, common anode displays are more popular as many logic circuits can sink more current than they can source. Also note that a common cathode display is not a direct replacement in a circuit for a common anode display and vice versa, as it is the same as connecting the LEDs in reverse, and hence light emission will not take place.
Depending upon the decimal digit to be displayed, the particular set of LEDs is forward biased. For instance, to display the numerical digit 0, we will need to light up six of the LED segments corresponding to a, b, c, d, e and f. Then the various digits from 0 through 9 can be displayed using a 7-segment display as shown below.
![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/all_numbers.jpg "Virtual Labs")
all_numbers

 <Image source : www.electronics-tutorials.


Refer the table given below to know the hex values for displaying a specific number on the 7 segment display during the simulation.

![common_cathode_hex_values](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/common_cathode_hex_values.bmp "Virtual Labs")


<Image source : aimagin.com>

### ** Documentation about 8051 Microcontroller
8051 Overview.pdf**

-8051 Hardware Overview.pdf

-8051 Instruction Set.pdf


<div align="center"> 

# Microcontroller interfaced with display devices

</div>
<br>
Procedure

1. Go to the simulator section to perform an experiment.


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/Goto-comn.jpg "Virtual Labs")


2. Read all the instructions popping up from simulator window carefully.


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/read-comn.jpg "Virtual Labs")

3. Click on show samples button provided below. Understand the interfacing diagram and sample code.

![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/sample-comn.jpg"Virtual Labs")

![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/copycode-comn.jpg "Virtual Labs")

4. Copy program from samples and paste in Text editor in simulator window.


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/display_code.JPG "Virtual Labs")


5. Or write your own assembly language code in Text editor.



6. Select appropriate Port according to your code.


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/display_port.JPG "Virtual Labs")

7. To check syntax errors on each line,debug option is provided.


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/display_debug.JPG "Virtual Labs")


8. If your code output is depending on timing sequence, please use debug funtion.It will show changes in output step by step.


9. To get the final output, run the simulator after debugging and assembling the code with no error.


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/display_run.JPG "Virtual Labs")


10. Solve test questions given below the simulator section.


11. Submit the answers and a PDF will be generated.(rename it as per your Rollno and Experiment no.)


![img](http://vlabs.iitb.ac.in/vlabs-dev/labs/8051-Microcontroller-Lab/labs/images/procedure_images/print-comn.JPG "Virtual Labs")
