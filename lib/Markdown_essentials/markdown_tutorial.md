<u>**BOOTATHON**</u>
*KNOCKDOWN THE LOCKDOWN*
![Vlabs Logo](logo.jpg)
# Welcome to Vlabs
------- 
### Topic : Markdown Training 
#### Key points :
> 1. Introduction
>> - History
>> - Uses And Requirements.

> 2. Basic Syntax 
  

> 3. Lists and code writing (using Code Blocks).
> 4. Adding Links and Email address.
> 5. Tables.
> 6.  Adding images to Markdown.
> 7. Footnotes and Emoji Support.


--------------------------------
<span style="text-decoration:underline">**1. Introduction**</span>

***History***

<p> Markdown is a lightweight markup language that you can use to add formatting elements to plain text  documents. Created by John Gruber in 2004, Markdown is now one of the world’s most popular markup languages.</p>


Using Markdown doesn't mean that you can't also use HTML. You can add HTML tags to any Markdown file. This is helpful if you prefer certain HTML tags to Markdown syntax. For example, some people find that it's easier to use HTML tags for images. <br> Markdown can be used for everything. People use it to create websites, documents, notes, books, presentations, email messages, and technical documentation.

***Uses And Requirements***
- At Vlabs we use .md files simply because it is open source. Which means it is " <b>free</b>  <i>free</i>  <u>free</u> " and others require license.
* Easy to code and understand.
- Easy to **copy** & *paste* contents as it is.

<u> **<span style="color:blue"> 2. Basic Syntax </span>** </u>

> - Headings

To create a heading, add number signs (#) in front of a word or phrase. The number of number signs you use should correspond to the heading level. For example,
# Heading 1 (biggest)
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6 (smallest)
####### (Hey I dont exist)

<h1> Heading 1 (biggest)</h1> 
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6 (smallest)</h6>
<h7> I am not bold so, i dont exist </h7>
<br><br>

> - Italic Text

To italicize text, add one asterisk or underscore before and after a word or phrase. To italicize the middle of a word for emphasis, add one asterisk without spaces around the letters.
Examples :-
*This text is in italics.*
_This also gives italic text_.
Watch for the word *italics*.
<i> I can also do the same job </i>
<br>

> - Bold Text

To bold text, add two asterisks or underscores before and after a word or phrase. To bold the middle of a word for emphasis, add two asterisks without spaces around the letters.
Examples :-
**This text is in bold.**
__This also gives bold text__.
Watch for the word **BOLD**.
<b> I can also do the same job </b>
<br>

> - Line Breaks

To create a line break (<br>), end a line with two or more spaces, and then type return.
<br>

> - Blockquotes

To create a blockquote, add a > in front of a paragraph.
<br>

> - Nested Blockquotes

Blockquotes can be nested. Add a >> in front of the paragraph you want to nest.
<br>

<u> **<span style="color:blue"> 3. Lists and Code Writing </span>** </u>

> - Lists

To create an ordered list, add line items with numbers followed by periods. The numbers don’t have to be in numerical order, but the list should start with the number one.
Examples :-
1. Remember to start with number one.
1. See it gives number two.

To create an unordered list, add dashes (-), asterisks (*), or plus signs (+) in front of line items. Indent one or more items to create a nested list.
Examples :-
* Unordered list type 1.
- Unordered list type 2.
+ Unordered list type 3.
<br>

> - Code Blocks

Code blocks are normally indented four spaces or one tab. When they’re in a list, indent them eight spaces or two tabs.

         <html>
         <head>
            <title>Test</title>
         </head>

<br>

<u> **<span style="color:blue"> 4. Adding Links and Email address </span>** </u>

> - Adding Links

To create a link, enclose the link text in brackets (e.g., [Vlabs IITB]) and then follow it immediately with the URL in parentheses (e.g., (http://vlabs.iitb.ac.in/vlab/)). You can optionally add a title for a link. This will appear as a tooltip when the user hovers over the link. To add a title, enclose it in parentheses after the URL.

Example :  [Vlabs IITB](http://vlabs.iitb.ac.in/vlab "Click to view labs")

<br>

> - Email Address And URLs

To quickly turn a URL or email address into a link, enclose it in angle brackets.

<support@vlab.co.in>
support@vlab.co.in
`http://www.vlab.co.in`

<br>

<u> **<span style="color:blue"> 5. Tables </span>** </u>


> - Tables

Tables are very important in Markdown. Everybody must be confident with them, as they are essential in Vlabs Documentation.

To add a table, use three or more hyphens (---) to create each column’s header, and use pipes (|) to separate each column. You can optionally add pipes on either end of the table.

Example :-

| Name | Place | Animal | Thing |
| ----| ------ |--------|-------|
| Shardul | Wardha| Lion | Home |
| PVGs COET | Pune | -----|Vlabs|
 
You can align text in the columns to the left, right, or center by adding a colon " : " to the left, right, or on both side of the hyphens within the header row.

| Name | Place | Animal | Thing | 
|:---:|:------:|:--------:|:-------:|
| Shardul | Wardha| Lion | Home |
| PVGs COET | Pune |      |Vlabs|
 
<br>

<u> **<span style="color:blue"> 4. Adding images to Markdown </span>** </u>



<br>

> - Images

To add an image, add an exclamation mark (!), followed by alt text in brackets, and the path or URL to the image asset in parentheses. You can optionally add a title after the URL in the parentheses.
 
Example :- 
![Vlabs Logo](bootathon2020.jpg "Next Bootathon at DY Patil, Mumbai")
![Vlabs Bootathon](images/vlabs.png "Last Bootcamp")

To add a link to an image, enclose the Markdown for the image in brackets, and then add the link in parentheses.


![Thank You](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")



> - Footnotes 

Footnotes allow you to add notes and references without cluttering the body of the document. When you create a footnote, a superscript number with a link appears where you added the footnote reference. Readers can click the link to jump to the content of the footnote at the bottom of the page.

To create a footnote reference, add a caret and an identifier inside brackets ([^1]). Identifiers can be numbers or words, but they can’t contain spaces or tabs. Identifiers only correlate the footnote reference with the footnote itself — in the output, footnotes are numbered sequentially.
 
Example:-

Check at the bottom of the page. [^3]
[^3]: This is the footnote.

> - Emoji Support :smile:

You can also add emojis to your Markdown file. (**Isn't that great** :unamused:)
Example :- :sweat:
You may get more on :- <https://gist.github.com/roachhd/1f029bd4b50b8a524f3c>



##~~THANk YOU~~
> - By :- Shardul Sayankal [S.E MECH]



[^note1]: This chapter contents Simple Markdown Requirements. 
