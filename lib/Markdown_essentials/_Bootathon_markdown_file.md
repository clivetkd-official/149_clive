<u> **<span style="color:blue"> 2. Basic Syntax </span>** </u>

> - Headings

To create a heading, add sign (#) in front of a word or phrase. The number of signs you use would determine the heading level. 

# Heading 1
### Heading 3
###### Heading 6


HTML

<h1> Heading 1 </h1>
<h6> Heading 6 </h6>


<br>

> - Italic Text

To make the text italic , add one asterisk or underscore before and after a word or phrase. To italicize the middle of a word for emphasis, add one asterisk without any space around the letters.

*This line is Italic*
_This line is Italic_
Watch out for the word *Italic*


HTML

<i> This line is Italic </i>

<br>

> - Bold Text

To bold text, add two asterisks or underscores before and after a word or phrase. To bold the middle of a word for emphasis, add two asterisks without spaces around the letters.

**This line is Bold**
__This line is Bold__
Watch out for the word **Bold**

HTML

<b> This line is Bold </b>

<br>

> - Line Breaks

To create a line break (<br>), complete the line and then type return.

<br>

> - Blockquotes

To create a blockquote, add a > in front of a paragraph.

> This is a blockquote


<br>

> - Nested Blockquotes

Blockquotes can be nested. Add a >> in front of the paragraph you want to nest.

> This is a blockquote.
>> This is a nested blockquote.



<br>

<u> **<span style="color:blue">  3. Lists </span>** </u>

> - Lists

To create an ordered list, add line items with numbers followed by periods. The numbers don’t have to be in numerical order, but the list should start with the number one.

1. The list must start with one.
1. Watch out for number two


To create an unordered list, add dashes (-), asterisks (*), or plus signs (+) in front of line items. Indent one or more items to create a nested list.

- This is the first type of unordered list
* This is the second type of unordered list
+ This is the third type of unordered list

<u> **<span style="color:blue"> 4. Adding Links and Email address </span>** </u>

> - Adding Links

To create a link, enclose the link text in brackets (e.g., [Vlabs IITB]) and then follow it immediately with the URL in parentheses (e.g., (http://vlabs.iitb.ac.in/vlab/)). You can optionally add a title for a link. This will appear as a tooltip when the user hovers over the link. To add a title, enclose it in parentheses after the URL.

Example :  [Link Text](link "Title")

 [VLabs](http://vlabs.iitb.ac.in/vlab/ "Click Here")


<br>

> - Email Address And URLs

To quickly turn a URL or email address into a link, enclose it in angle brackets.

<support@vlab.co.in>
support@vlab.co.in

To disable a link
`http://www.vlab.co.in`

<br>

<u> **<span style="color:blue"> 5. Tables </span>** </u>


> - Tables

Tables are very important in Markdown. Everybody must be confident with them, as they are essential in Vlabs Documentation.

To add a table, use three or more hyphens (---) to create each column’s header, and use pipes (|) to separate each column. You can optionally add pipes on either end of the table.

|Name|Roll no.|class|
|----|----|----|
|Shardul|8089|SE|
|Paras|8090|BE|


You can align text in the columns to the left, right, or center by adding a colon " : " to the left, right, or on both side of the hyphens within the header row.

|Name|Roll no.|class|
|:----|----:|:----:|
|Shardul|8089|SE|
|Paras|8090|BE|

<br>

<u> **<span style="color:blue"> 6. Adding images to Markdown </span>** </u>



<br>

> - Images

To add an image, add an exclamation mark (!), followed by text in square brackets, and the path or URL to the image in parentheses. You can optionally add a title after the URL in the parentheses.
 
Example :- 
![Image name](url or path "title")
![Bootathon](logo.jpg "Virtual Labs")
![photo](images/vlabs.png)


To add a link to an image, enclose the Markdown for the image in brackets, and then add the link in parentheses.


![Thank You](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")


----



> - By :- Shardul Sayankal [S.E MECH]




